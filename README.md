# get_node

在x64架构的linux上快速安装node的lts版.

## 为什么

国内安装node很难,下载太慢.

## 怎样使用

```shell
curl -L https://gitee.com/hbybyyang/get_node/raw/master/run.sh | bash
```

## TODO

- 自动识别架构(x64或ARMv7或ARMv8)

## 其他

目前白嫖阿里Code的文件托管,不知道能用多久.

如果有免费靠谱的CDN请推荐给我.
