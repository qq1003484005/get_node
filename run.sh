#!/usr/bin/env bash
# curl -L https://gitee.com/hbybyyang/get_node/raw/master/run.sh | bash

set -e -o pipefail

echo '下载...'
export downloadUlr='http://prod1.wos.58dns.org/StQpOnMStWv/vrspeechaudiowos/node-v10.15.2-linux-x64.tar.gz'
mkdir -p /tmp/get_node
cd /tmp/get_node
curl -o node-v10.15.2-linux-x64.tar.xz `echo $downloadUlr`

echo '解压...'
tar -xf node-v10.15.2-linux-x64.tar.xz

echo '安装...'
/bin/cp -rf /tmp/get_node/node-v10.15.2-linux-x64/bin/* /usr/local/bin/
/bin/cp -rf /tmp/get_node/node-v10.15.2-linux-x64/include/* /usr/local/include/
/bin/cp -rf /tmp/get_node/node-v10.15.2-linux-x64/lib/* /usr/local/lib/
/bin/cp -rf /tmp/get_node/node-v10.15.2-linux-x64/share/* /usr/local/share/

echo '换源...'
npm config set registry https://registry.npm.taobao.org

echo '清理...'
rm -rf /tmp/get_node

echo '刷新环境...'
cd ~
source ~/.bashrc
node -v
